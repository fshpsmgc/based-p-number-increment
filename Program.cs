using System;
using static LongInteger;

public class Program{
	static public void Main(string[] args){
		if(args.Length != 2){
			Console.WriteLine("Использование программы: <имя программы> <исходное число> <целочисленное основание>");
		}else{
			LongInteger integer = new LongInteger();
			integer.FromString(args[0]);
			integer.Base = Convert.ToInt32(args[1]);
			
			integer.Print(" + 1 = ");
			integer.Increment();
			integer.PrintLine();
		}

	}
}
