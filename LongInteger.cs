using System;

public class LongInteger{
	public int[] Numbers;
	public int Base;

	public void Increment(){
		int i = Numbers.Length - 1;
		while(Numbers[i] + 1 >= Base && i > 0){
			Numbers[i] = Numbers[i] + 1 - Base;
			i--;
		}

		if(i == 0 && (Numbers.Length != 1 || Base == 2)){
			Array.Resize(ref Numbers, Numbers.Length + 1);
			for(int j = Numbers.Length - 1; j > 0; j--){
				Numbers[j] = Numbers[j - 1];
			}
			Numbers[1] = 0;
			Numbers[0] = 0;
		}
		
		Numbers[i]++;
		

		
	}

	public void FromString(string str){
		Numbers = new int[str.Length];
		for(int i = 0; i < str.Length; i++){
			Numbers[i] = Int32.Parse(str[i].ToString());
		}
	}

	public void Print(string addString = ""){
		for(int i = 0; i < Numbers.Length; i++){
			Console.Write(Numbers[i]);
		}
		Console.Write(addString);
	}

	public void PrintLine(string addString = ""){
		for(int i = 0; i < Numbers.Length; i++){
			Console.Write(Numbers[i]);
		}
		Console.Write(addString);
		Console.WriteLine();
	}

}